﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using MySql.Data.MySqlClient;
using MySQLDemo.Classes;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace GuiAssessment3_10000246
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            Names.ItemsSource = loadData();
        }

        static List<string> loadData()
        {
            var command = $"Select FNAME from tbl_people";
            var list = MySQLCustom.ShowInList(command);
            return list;
        }

        void selectedName(string name)
        {
            var command = $"Select * from tbl_people WHERE FNAME = '{name}' ";
            var b = MySQLCustom.ShowInList(command);

            UserID.Text = b[0];
            inputFirstName.Text = b[1];
            inputSurname.Text = b[2];
            inputDOB.Text = b[3];

            inputStreetNo.Text = b[4];
            inputStreetName.Text = b[5];
            inputPostCode.Text = b[6];
            inputTownCity.Text = b[7];

            inputPhoneNo1.Text = b[8];
            inputPhoneNo2.Text = b[9];
            inputEmail.Text = b[10];
        }

        void clearAll()
        {
            UserID.Text = "";
            inputFirstName.Text = "";
            inputSurname.Text = "";
            inputDOB.Text = "";

            inputStreetNo.Text = "";
            inputStreetName.Text = "";
            inputPostCode.Text = "";
            inputTownCity.Text = "";

            inputPhoneNo1.Text = "";
            inputPhoneNo2.Text = "";
            inputEmail.Text = "";
        }

        static async void messageBox(string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = "Checking...";
            dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            var res = await dialog.ShowAsync();

            if ((int)res.Id == 0)
            { }
        }

        private void Names_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Names.SelectedItem != null)
            {
                selectedName(Names.SelectedItem.ToString());
            }
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            Splitter.IsPaneOpen = !Splitter.IsPaneOpen;
        }

        private async void DetailsAdd_Click(object sender, RoutedEventArgs e)
        {
            if (inputFirstName.Text == "" || inputSurname.Text == "" || inputDOB.Text == "" || inputStreetNo.Text == "" || inputStreetName.Text == "" || inputPostCode.Text == "" || inputTownCity.Text == "" || inputPhoneNo1.Text == "" || inputPhoneNo2.Text == "" || inputEmail.Text == "")
            {
                var message = "All fields must be correctly filled before you can add a user to the database.";
                var messageBox = new MessageDialog($"{message}");
                messageBox.Title = "An error has occured. Please try again.";
                messageBox.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await messageBox.ShowAsync();
            }
            else
            {
                MySQLCustom.AddData(inputFirstName.Text, inputSurname.Text, inputDOB.Text, inputStreetNo.Text, inputStreetName.Text, inputPostCode.Text, inputTownCity.Text, inputPhoneNo1.Text, inputPhoneNo2.Text, inputEmail.Text);
                Names.ItemsSource = loadData();
                clearAll();
            }
        }

        private async void DetailsUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (inputFirstName.Text == "" || inputSurname.Text == "" || inputDOB.Text == "" || UserID.Text == "" || inputStreetNo.Text == "" || inputStreetName.Text == "" || inputPostCode.Text == "" || inputTownCity.Text == "" || inputPhoneNo1.Text == "" || inputPhoneNo2.Text == "" || inputEmail.Text == "")
            {
                var message = "Select a user and edit the data to update the database. All fields must be correctly filled.";
                var messageBox = new MessageDialog($"{message}");
                messageBox.Title = "An error has occured. Please try again.";
                messageBox.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await messageBox.ShowAsync();
            }
            else
            { 
                MySQLCustom.UpdateData(inputFirstName.Text, inputSurname.Text, inputDOB.Text, inputStreetNo.Text, inputStreetName.Text, inputPostCode.Text, inputTownCity.Text, inputPhoneNo1.Text, inputPhoneNo2.Text, inputEmail.Text, UserID.Text);
                Names.ItemsSource = loadData();
                clearAll();
            }
        }

        private async void DetailsRemove_Click(object sender, RoutedEventArgs e)
        {
            if (UserID.Text == "")
            {
                var message = "You must select a user before you can remove data from the database.";
                var messageBox = new MessageDialog($"{message}");
                messageBox.Title = "An error has occured. Please try again.";
                messageBox.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await messageBox.ShowAsync();
            }
            else
            {
                MySQLCustom.DeleteDate(UserID.Text);
                Names.ItemsSource = loadData();
                clearAll();
            }
        }

        private async void AddressAdd_Click(object sender, RoutedEventArgs e)
        {
            if (inputFirstName.Text == "" || inputSurname.Text == "" || inputDOB.Text == "" || UserID.Text == "" || inputStreetNo.Text == "" || inputStreetName.Text == "" || inputPostCode.Text == "" || inputTownCity.Text == "" || inputPhoneNo1.Text == "" || inputPhoneNo2.Text == "" || inputEmail.Text == "")
            {
                var message = "All fields must be correctly filled before you can add a user to the database.";
                var messageBox = new MessageDialog($"{message}");
                messageBox.Title = "An error has occured. Please try again.";
                messageBox.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await messageBox.ShowAsync();
            }
            else
            {
                MySQLCustom.AddData(inputFirstName.Text, inputSurname.Text, inputDOB.Text, inputStreetNo.Text, inputStreetName.Text, inputPostCode.Text, inputTownCity.Text, inputPhoneNo1.Text, inputPhoneNo2.Text, inputEmail.Text);
                Names.ItemsSource = loadData();
                clearAll();
            }
        }

        private async void AddressUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (inputFirstName.Text == "" || inputSurname.Text == "" || inputDOB.Text == "" || UserID.Text == "" || inputStreetNo.Text == "" || inputStreetName.Text == "" || inputPostCode.Text == "" || inputTownCity.Text == "" || inputPhoneNo1.Text == "" || inputPhoneNo2.Text == "" || inputEmail.Text == "")
            {
                var message = "Select a user and edit the data to update the database. All fields must be correctly filled.";
                var messageBox = new MessageDialog($"{message}");
                messageBox.Title = "An error has occured. Please try again.";
                messageBox.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await messageBox.ShowAsync();
            }
            else
            {
                MySQLCustom.UpdateData(inputFirstName.Text, inputSurname.Text, inputDOB.Text, inputStreetNo.Text, inputStreetName.Text, inputPostCode.Text, inputTownCity.Text, inputPhoneNo1.Text, inputPhoneNo2.Text, inputEmail.Text, UserID.Text);
                Names.ItemsSource = loadData();
                clearAll();
            }
        }

        private async void AddressRemove_Click(object sender, RoutedEventArgs e)
        {
            if (UserID.Text == "")
            {
                var message = "You must select a user before you can remove data from the database.";
                var messageBox = new MessageDialog($"{message}");
                messageBox.Title = "An error has occured. Please try again.";
                messageBox.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await messageBox.ShowAsync();
            }
            else
            {
                MySQLCustom.DeleteDate(UserID.Text);
                Names.ItemsSource = loadData();
                clearAll();
            }
        }

        private async void ContactAdd_Click(object sender, RoutedEventArgs e)
        {
            if (inputFirstName.Text == "" || inputSurname.Text == "" || inputDOB.Text == "" || UserID.Text == "" || inputStreetNo.Text == "" || inputStreetName.Text == "" || inputPostCode.Text == "" || inputTownCity.Text == "" || inputPhoneNo1.Text == "" || inputPhoneNo2.Text == "" || inputEmail.Text == "")
            {
                var message = "All fields must be correctly filled before you can add a user to the database.";
                var messageBox = new MessageDialog($"{message}");
                messageBox.Title = "An error has occured. Please try again.";
                messageBox.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await messageBox.ShowAsync();
            }
            else
            {
                MySQLCustom.AddData(inputFirstName.Text, inputSurname.Text, inputDOB.Text, inputStreetNo.Text, inputStreetName.Text, inputPostCode.Text, inputTownCity.Text, inputPhoneNo1.Text, inputPhoneNo2.Text, inputEmail.Text);
                Names.ItemsSource = loadData();
                clearAll();
            }
        }

        private async void ContactUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (inputFirstName.Text == "" || inputSurname.Text == "" || inputDOB.Text == "" || UserID.Text == "" || inputStreetNo.Text == "" || inputStreetName.Text == "" || inputPostCode.Text == "" || inputTownCity.Text == "" || inputPhoneNo1.Text == "" || inputPhoneNo2.Text == "" || inputEmail.Text == "")
            {
                var message = "Select a user and edit the data to update the database. All fields must be correctly filled.";
                var messageBox = new MessageDialog($"{message}");
                messageBox.Title = "An error has occured. Please try again.";
                messageBox.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await messageBox.ShowAsync();
            }
            else
            {
                MySQLCustom.UpdateData(inputFirstName.Text, inputSurname.Text, inputDOB.Text, inputStreetNo.Text, inputStreetName.Text, inputPostCode.Text, inputTownCity.Text, inputPhoneNo1.Text, inputPhoneNo2.Text, inputEmail.Text, UserID.Text);
                Names.ItemsSource = loadData();
                clearAll();
            }
        }

        private async void ContactRemove_Click(object sender, RoutedEventArgs e)
        {
            if (UserID.Text == "")
            {
                var message = "You must select a user before you can remove data from the database.";
                var messageBox = new MessageDialog($"{message}");
                messageBox.Title = "An error has occured. Please try again.";
                messageBox.Commands.Add(new UICommand { Label = "Close", Id = 0 });
                var res = await messageBox.ShowAsync();
            }
            else
            {
                MySQLCustom.DeleteDate(UserID.Text);
                Names.ItemsSource = loadData();
                clearAll();
            }
        }
    }
}
